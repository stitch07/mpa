// dynamic_bitset.hh
// Minimal dynamic bitset optimized for storage and speed effeciency

#ifndef _DYNAMIC_BITSET_HH_
#define _DYNAMIC_BITSET_HH_

#include "bitmask.hh"
#include <concepts>
#include <type_traits>

// Dynamically allocated bitset.
class DynamicBitSet
{
  public:
    // Construct as container.
    DynamicBitSet(std::size_t size = 0,
                  std::size_t starting_capacity = 64);
    /*
     * Construct from any type.
     * Only explicit if source type is not arithmetic.
     */
    template <class T>
    explicit(!std::is_arithmetic_v<T>) DynamicBitSet(T const &val);
    // Copy constructor, does deepcopy.
    DynamicBitSet(DynamicBitSet const &val);
    // Move constructor.
    DynamicBitSet(DynamicBitSet &&val) noexcept;

    // Copy assignment, does deepcopy.
    DynamicBitSet &operator=(DynamicBitSet const &val);
    // Move assignment.
    DynamicBitSet &operator=(DynamicBitSet &&val) noexcept;

    ~DynamicBitSet();

    // Get individual bit at index.
    bool getbit(std::size_t index) const;
    // Set individual bit at index.
    void setbit(std::size_t index, bool val);

    /*
     * Increase DynamicBitSet capacity.
     * Grow the container if the new capacity is larger than current cpacity.
     * Else do nothing.
     */
    void reserve(std::size_t capacity);
    // Shrink DynamicBitSet capacity to match its size.
    void shrink_to_fit();

    // Push bit to end of container.
    void push_back(bool bit) noexcept;
    // Pop bit from end of container.
    // Throws std::logic_error if container is empty.
    bool pop_back();

    // Get container capacity.
    std::size_t capacity() noexcept;
    // Returns true if container is empty.
    bool empty() noexcept;

    // Empties the container.
    void clear() noexcept;

    bool operator[](std::size_t index) const;
    bitmask<> operator[](std::size_t index);

    DynamicBitSet operator~() const noexcept;

    DynamicBitSet operator|(DynamicBitSet const &other) const noexcept;
    DynamicBitSet operator&(DynamicBitSet const &other) const noexcept;
    DynamicBitSet operator^(DynamicBitSet const &other) const;

    DynamicBitSet operator>>(std::size_t shifts) const noexcept;
    DynamicBitSet operator<<(std::size_t shifts) const noexcept;

    DynamicBitSet operator|=(DynamicBitSet const &other);
    DynamicBitSet operator&=(DynamicBitSet const &other);
    DynamicBitSet operator^=(DynamicBitSet const &other);

    DynamicBitSet operator>>=(std::size_t shift) noexcept;
    DynamicBitSet operator<<=(std::size_t shift) noexcept;

  private:
    std::size_t _size;
    std::size_t _capacity;
    std::byte *chunks;
};

#endif // _DYNAMIC_BITSET_HH_
