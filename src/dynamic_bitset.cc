#include "dynamic_bitset.hh"
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <utility>

DynamicBitSet::DynamicBitSet(std::size_t size,
                             std::size_t starting_capacity)
    : _size(size)
{
    // If starting_capacity is lesser than size
    // Make starting capacity twice the size
    if (starting_capacity < size)
    {
        starting_capacity = size * 2;
    }

    std::size_t chunk_quantity =
        static_cast<int>(std::ceil(static_cast<double>(starting_capacity) / 8));

    _capacity = chunk_quantity * 8;
    chunks = new std::byte[chunk_quantity];
}

template <class T>
DynamicBitSet::DynamicBitSet(T const &val)
    : _size(sizeof(val) * 8),
      _capacity(sizeof(val) * 8)
{
    chunks = new std::byte[sizeof(val)];
    std::memcpy(chunks, &val, sizeof(val));
}

DynamicBitSet::DynamicBitSet(DynamicBitSet const &val)
    : _size(val._size),
      _capacity(val._capacity)
{
    chunks = new std::byte[_capacity / 8];
    std::memcpy(chunks, val.chunks, _capacity / 8);
}

DynamicBitSet::DynamicBitSet(DynamicBitSet &&val) noexcept
    : _size(std::exchange(val._size, 0)),
      _capacity(std::exchange(val._capacity, 0)),
      chunks(std::exchange(val.chunks, nullptr))
{
}

DynamicBitSet &DynamicBitSet::operator=(DynamicBitSet const &val)
{
    _size = val._size;
    _capacity = val._capacity;
    chunks = new std::byte[_capacity / 8];
    std::memcpy(chunks, val.chunks, _capacity / 8);
    return *this;
}

DynamicBitSet &DynamicBitSet::operator=(DynamicBitSet &&val) noexcept
{
    _size = std::exchange(val._size, 0);
    _capacity = std::exchange(val._capacity, 0);
    chunks = std::exchange(val.chunks, nullptr);
    return *this;
}

DynamicBitSet::~DynamicBitSet()
{
    delete[] chunks;
}

bool DynamicBitSet::getbit(std::size_t index) const
{
    if (index >= _size)
    {
        throw std::out_of_range("Attempting to access bitset element "
                                "beyond range");
    }

    std::size_t chunk_index = std::floor(index / 8);
    std::uint_fast8_t bit_index = index % 8;

    return static_cast<bool>((chunks[chunk_index] >> bit_index) & std::byte{1});
}

void DynamicBitSet::setbit(std::size_t index, bool val)
{
    if (index >= _size)
    {
        throw std::out_of_range("Attempting to access bitset element "
                                "beyond range");
    }

    std::size_t chunk_index = std::floor(index / 8);
    std::uint_fast8_t bit_index = index % 8;

    chunks[chunk_index] = ((chunks[chunk_index] & ~(std::byte{1} << bit_index)) | (std::byte{val} << bit_index));
}

bool DynamicBitSet::operator[](std::size_t index) const
{
    return getbit(index);
}

bitmask<> DynamicBitSet::operator[](std::size_t index)
{
    if (index >= _size)
    {
        throw std::out_of_range("Attempting to access bitset element "
                                "beyond range");
    }

    std::size_t chunk_index = std::floor(index / 8);
    std::uint_fast8_t bit_index = index % 8;

    return bitmask(chunks[chunk_index], bit_index);
}
