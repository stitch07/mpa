#ifndef _BITMASK_HH_
#define _BITMASK_HH_

#include <cstddef>
#include <cstdint>
#include <type_traits>

// Class to mask reference to individual bit
template <bool is_const = false>
class bitmask
{
  public:
    using ChunkType = std::conditional_t<is_const, std::byte const, std::byte>;

    bitmask(ChunkType &chunk, std::uint_fast8_t index) noexcept
        : _chunk(chunk), _index(index){};
    bitmask(bitmask const &source) = default;

    operator bool() const noexcept
    {
        return static_cast<bool>((_chunk >> (7 - _index)) & std::byte{1});
    }

    template <typename = std::enable_if_t<!is_const>>
    bitmask &operator=(bool val) noexcept
    {
        _chunk = ((_chunk & ~(std::byte{1} << (7 - _index))) |
                  (std::byte{val} << (7 - _index)));
        return *this;
    }

  private:
    ChunkType &_chunk;
    std::uint_fast8_t const _index;
};

bitmask(std::byte &, std::uint_fast8_t)->bitmask<false>;
bitmask(std::byte const &, std::uint_fast8_t)->bitmask<true>;

#endif
